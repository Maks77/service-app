import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'
import uuid from 'uuid/v1'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.prototype.$uuid = uuid

Vue.use(Vuetify, {
  iconfont: 'mdi'
})

/* eslint-disable no-new */
new Vue({
  components: { App },
  vuetify: new Vuetify(),
  router,
  store,
  template: '<App/>'
}).$mount('#app')
